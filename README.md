What is ccnet
====
[Ccnet](https://github.com/haiwen/ccnet) is a framework for writing networked applications in C

What is go-ccnet
===

go-ccnet is the client library for accessing ccnet service in [golang](www.golang.org)

Example code

===

See ccnet_test.go for a basic example.

