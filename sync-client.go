package ccnet

// Synchronous mode client. see client.go for detailed comments.

import (
	"fmt"
)

type SyncClient struct {
	Client
	Response Response
	reqId    int
}

func NewSyncClient(daemon_port int) (c *SyncClient) {
	c = new(SyncClient)
	c.daemon_port = daemon_port
	c.reqId = 1000
	return c
}

func (c *SyncClient) DisconnectDaemon() {
	if !c.Connected {
		return
	}
	c.conn.Close()
	c.Connected = false
}

func (c *SyncClient) ReadResponse() (err error) {
	pkt, err := readPacket(c.reader)
	if err != nil {
		return
	}

	if pkt.typ != CCNET_MSG_RESPONSE {
		err = fmt.Errorf("invalid packet type %d", pkt.typ)
		return
	}

	code, code_msg, content, err := parseResponse(pkt.body)
	if err != nil {
		return
	} else {
		c.Response.Code = code
		c.Response.CodeMsg = code_msg
		c.Response.Content = content
	}

	return
}

func (c *SyncClient) SendCmd(cmd string) (err error) {
	id := c.GetRequestId()
	c.SendRequest(id, "receive-cmd")
	err = c.ReadResponse() // block waiting for response
	if err != nil {
		return
	}

	resp := &c.Response
	if resp.Code != "200" {
		err = fmt.Errorf("bad response: %s %s", resp.Code, resp.CodeMsg)
		return
	}

	err = c.SendUpdate(id, "200", "", []byte(cmd+"\000"))
	if err != nil {
		return
	}

	fmt.Printf("send a commad, id = %d, %s\n", id, cmd)

	err = c.ReadResponse()
	return
}

func (c *SyncClient) GetRequestId() int {
	c.reqId += 1
	return c.reqId
}
