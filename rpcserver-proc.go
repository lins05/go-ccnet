package ccnet

import (
	"ccnet/searpc"
	"fmt"
	"log"
)

var _ = fmt.Println
var _ = log.Println

const (
	maxTransferLength = 64000
)

type RpcServerProc struct {
	SlaveProcessor
	fcallStr []byte
	remain   []byte
}

func (proc *RpcServerProc) Start(argv []string) (err error) {
	proc.SendResponse("200", "OK", nil)
	return
}

func (proc *RpcServerProc) sendRet() {
	remain := proc.remain
	l := len(remain)
	if l < maxTransferLength {
		buf := make([]byte, l+1)
		copy(buf[:l], remain)
		buf[l] = '\000'
		proc.SendResponse(SC_SERVER_RET, SS_SERVER_RET, buf)
	} else {
		proc.SendResponse(SC_SERVER_MORE, SS_SERVER_MORE, proc.remain[:maxTransferLength])
		proc.remain = remain[maxTransferLength:]
	}
}

func (proc *RpcServerProc) HandleUpdate() {
	for r := range proc.Channel {
		code, content := r.Code, r.Content
		if code == SC_CLIENT_CALL_MORE {
			proc.fcallStr = append(proc.fcallStr, content...)
		} else if code == SC_CLIENT_CALL {
			proc.fcallStr = append(proc.fcallStr, content...)
			svcName := proc.Name
			proc.remain = searpc.ServerCallFunction(svcName, proc.fcallStr)
			proc.fcallStr = nil

			proc.sendRet()
		} else if code == SC_CLIENT_MORE {
			proc.sendRet()
		}
	}
}
