package ccnet

/************************************************************************

 client.go implements basic ccnet client operations.

 This file implements common client operations. sync-client.go and
 async-client.go implements their specific operations.

 Sync mode client: after send a request, must block wait for response. A
 SyncClient can only be used to access service provieded by others, i.e. it
 can't be used as a service provider.

 Async mode client: after send a request/respones, do not need to wait for
 response. For this reason, all application that want to provide their own
 service must create an async client.


***********************************************************************/

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"net"
	"sync"
)

type Client struct {
	MyPeerId    string
	daemon_port int
	Connected   bool
	conn        net.Conn
	reader      io.Reader
	writer      *bufio.Writer
	// To enable safety of multiple concurrent call of
	// SendResponse/SendUpdate, write needs to be locked, since bufio.Writer
	// is not goroutine-safe
	wrlock sync.Mutex
}

type Response struct {
	Code, CodeMsg string
	Content       []byte
}

type Update struct {
	Code, CodeMsg string
	Content       []byte
}

func (c *Client) ConnectDaemon() (err error) {
	addr := fmt.Sprintf("localhost:%d", c.daemon_port)
	conn, err := net.Dial("tcp", addr)
	if err != nil {
		return
	}

	c.conn = conn
	c.reader = conn.(io.Reader)
	c.writer = bufio.NewWriter(conn)
	c.Connected = true
	return
}

func (c *Client) SendRequest(req_id int, request string) (err error) {
	pkt := &packet{
		typ:  CCNET_MSG_REQUEST,
		id:   req_id,
		body: []byte(request),
	}

	err = writePacket(c.writer, pkt)
	return
}

func formatBody(code, code_msg string, content []byte) []byte {
	buf := new(bytes.Buffer)
	buf.WriteString(code)
	if len(code_msg) != 0 {
		buf.WriteString(" " + code_msg + "\n")
	} else {
		buf.WriteString("\n")
	}

	if content != nil {
		buf.Write(content)
	}

	return buf.Bytes()
}

func (c *Client) SendResponse(req_id int, code, code_msg string, content []byte) (err error) {
	body := formatBody(code, code_msg, content)
	pkt := &packet{
		typ:  CCNET_MSG_RESPONSE,
		id:   req_id,
		body: body,
	}

	c.wrlock.Lock()
	err = writePacket(c.writer, pkt)
	c.wrlock.Unlock()
	return
}

func (c *Client) SendUpdate(req_id int, code, code_msg string, content []byte) (err error) {
	body := formatBody(code, code_msg, content)
	pkt := &packet{
		typ:  CCNET_MSG_UPDATE,
		id:   req_id,
		body: body,
	}

	c.wrlock.Lock()
	err = writePacket(c.writer, pkt)
	c.wrlock.Unlock()
	return
}

func parseResponse(body []byte) (code, code_msg string, content []byte, err error) {
	if len(body) < 3 {
		err = fmt.Errorf("packet body too short")
		return
	}

	code = string(body[:3])
	if body[3] == '\n' {
		// no code msg
		code_msg = ""
		if len(body) > 4 {
			content = body[4:]
		}

	} else {
		if body[3] != ' ' {
			goto onerror
		}

		pos := bytes.IndexByte(body, '\n')
		if pos == -1 {
			goto onerror
		}

		code_msg = string(body[4:pos])
		if len(body) > pos+1 {
			content = body[pos+1:]
		}
	}

	return

onerror:
	err = fmt.Errorf("invaid response/update format")
	return
}
