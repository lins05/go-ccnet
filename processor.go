package ccnet

import (
	"fmt"
)

/************************************************************************

 client.go implements ccnet processor mechanism.

 master processor: service consumer, invokes local or remote ccnet service
 slave processor: service provider, passively invoked by local or remote peer


***********************************************************************/

var _ = fmt.Println

const (
	slave_mask      = 0x80000000
	request_id_mask = 0x7fffffff
)

// Generic processor type
type Processor struct {
	PeerId string
	Name   string
	Id     int
	Client *AsyncClient
	// when a processor is shutdown on error, Failure records the error
	Failure int
	done    bool
	// ensure only one goroutine are accessing the processor concurrently
}

// Generic processor interface
type IProcessor interface {
	Init(name string, id int, peerId string, c *AsyncClient)
	Start([]string) error

	Done(bool)
	ShutDown(int)

	// Get the underlying Processor from an IMasterProcessor/ISlaveProcessor
	// interface variable
	GetProcessor() *Processor
	String() string
}

func (proc *Processor) String() string {
	if isSlaveId(proc.Id) {
		return fmt.Sprintf("%s(-%d)", proc.Name, toPacketId(proc.Id))
	}

	return fmt.Sprintf("%s(%d)", proc.Name, proc.Id)
}

func (proc *Processor) Init(name string, id int, peerId string, c *AsyncClient) {
	proc.Name = name
	proc.Id = id
	proc.PeerId = peerId
	proc.Client = c
}

func (proc *Processor) GetProcessor() *Processor {
	return proc
}

func (proc *Processor) ShutDown(reason int) {
	proc.done = true
	proc.Client.removeProcessor(proc.Id)
}

func (proc *Processor) Done(bool) {
	proc.done = true
	proc.Client.removeProcessor(proc.Id)
}

// Slave processor type. Id of a slave processor is always negative (highest bit set)
type SlaveProcessor struct {
	Processor
	Channel chan *Update
}

type ISlaveProcessor interface {
	IProcessor

	ReceiveUpdate(*Update)
	SendResponse(string, string, []byte)

	// Slave processor specific interface
	HandleUpdate()
}

func (proc *SlaveProcessor) Init(name string, id int, peerId string, c *AsyncClient) {
	proc.Processor.Init(name, id, peerId, c)
	proc.Channel = make(chan *Update, 100)
}

func (proc *SlaveProcessor) SendResponse(code, code_msg string, content []byte) {
	proc.Client.SendResponse(toPacketId(proc.Id), code, code_msg, content)
}

func (proc *SlaveProcessor) ShutDown(code int) {
	if proc.Channel != nil {
		close(proc.Channel)
	}

	proc.Processor.ShutDown(code)
}

func (proc *SlaveProcessor) Done(success bool) {
	if proc.Channel != nil {
		close(proc.Channel)
	}

	proc.Processor.Done(success)
}

// Master processor type. Id of a master processor is always positive (highest bit clear)
type MasterProcessor struct {
	Processor
	Channel   chan *Response
	doneFuncs []func(*MasterProcessor, bool)
}

type IMasterProcessor interface {
	IProcessor

	ReceiveResponse(*Response)
	SendUpdate(string, string, []byte)

	// Master processor specific interface
	SendRequest(string)
	HandleResponse()
}

func (proc *MasterProcessor) Init(name string, id int, peerId string, c *AsyncClient) {
	proc.Processor.Init(name, id, peerId, c)
	proc.Channel = make(chan *Response, 100)
}

func (proc *MasterProcessor) SendRequest(request string) {
	proc.Client.SendRequest(toPacketId(proc.Id), request)
}

func (proc *MasterProcessor) SendUpdate(code, code_msg string, content []byte) {
	proc.Client.SendUpdate(toPacketId(proc.Id), code, code_msg, content)
}

func (proc *MasterProcessor) AddDoneFn(fn func(*MasterProcessor, bool)) {
	if fn == nil {
		fmt.Println("warning: trying to add nil done fn to processor", proc.Name)
	} else {
		if proc.doneFuncs == nil {
			// when the first done fn is added
			proc.doneFuncs = make([]func(*MasterProcessor, bool), 0, 3)
		}
		proc.doneFuncs = append(proc.doneFuncs, fn)
	}
}

func (proc *MasterProcessor) Done(success bool) {
	if success {
		// when a master proc is done successfully, notify slave to done too
		proc.SendUpdate(SC_PROC_DONE, SS_PROC_DONE, nil)
	}

	// run all done funcs
	for _, fn := range proc.doneFuncs {
		if fn != nil {
			fn(proc, success)
		}
	}

	if proc.Channel != nil {
		close(proc.Channel)
	}

	proc.Processor.Done(success)
}

func (proc *MasterProcessor) ShutDown(reason int) {
	success := (reason == PROC_DONE)
	if proc.Failure == PROC_NOTSET && reason != PROC_NOTSET {
		proc.Failure = reason
	}

	// run all done funcs
	for _, fn := range proc.doneFuncs {
		if fn != nil {
			fn(proc, success)
		}
	}

	if proc.Channel != nil {
		close(proc.Channel)
	}

	proc.Processor.ShutDown(reason)
}

func (proc *SlaveProcessor) ReceiveUpdate(u *Update) {
	proc.Channel <- u
}

func (proc *MasterProcessor) ReceiveResponse(r *Response) {
	proc.Channel <- r
}
