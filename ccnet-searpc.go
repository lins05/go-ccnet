package ccnet

/************************************************************************

 ccnet-searpc.go implements ccnet rpc client operation based on searpc
 framework.

***********************************************************************/

import (
	"ccnet/searpc"
	"fmt"
)

type SearpcTx struct {
	c       *SyncClient
	svcName string
}

// SearpcTx implements the searpc.Transporter interface
func (tx *SearpcTx) Transport(fcallData []byte) ([]byte, error) {
	var err error
	c := tx.c
	id := c.GetRequestId()

	c.SendRequest(id, tx.svcName)
	err = c.ReadResponse()
	if err != nil {
		return nil, err
	}

	resp := &c.Response
	if resp.Code != SC_OK {
		return nil, fmt.Errorf("%s %s", resp.Code, resp.CodeMsg)
	}

	l := len(fcallData)
	offset := 0
	for {
		remain := l - offset
		if remain <= maxTransferLength {
			c.SendUpdate(id, SC_CLIENT_CALL, SS_CLIENT_CALL, fcallData[offset:])
			break
		} else {
			c.SendUpdate(id, SC_CLIENT_CALL_MORE, SS_CLIENT_CALL_MORE, fcallData[offset:offset+maxTransferLength])
			offset += maxTransferLength
		}
	}

	var ret []byte
	for {
		err = c.ReadResponse()
		if err != nil {
			return nil, err
		}

		switch resp.Code {
		case SC_SERVER_MORE:
			l := len(resp.Content)
			if l == 0 {
				return nil, fmt.Errorf("bad response content")
			}
			ret = append(ret, resp.Content...)
			c.SendUpdate(id, SC_CLIENT_MORE, SS_CLIENT_MORE, nil)

		case SC_SERVER_RET:
			l := len(resp.Content)
			if l == 0 || resp.Content[l-1] != 0 {
				return nil, fmt.Errorf("bad response content")
			}
			ret = append(ret, resp.Content[:l-1]...)
			c.SendUpdate(id, SC_PROC_DONE, SC_PROC_DONE, nil)
			return ret, nil

		default:
			return nil, fmt.Errorf("bad response code")
		}
	}

	return nil, nil
}

func (c *SyncClient) MakeSearpcClient(svcName string) *searpc.Client {
	tx := &SearpcTx{
		c:       c,
		svcName: svcName,
	}

	return searpc.NewClient(tx)
}
