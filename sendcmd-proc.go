package ccnet

import ()

const (
	request_sent = 1
	connected    = 2
)

type SendCmdProc struct {
	MasterProcessor
	callback func(string, []byte)
	status   int
}

func (proc *SendCmdProc) Start(argv []string) (err error) {
	proc.SendRequest("receive-cmd")
	proc.status = request_sent
	return
}

func (proc *SendCmdProc) SetCallBack(cb func(string, []byte)) {
	proc.callback = cb
}

func (proc *SendCmdProc) SendCmd(cmd string) {
	proc.SendUpdate("200", "", []byte(cmd+"\000"))
}

func (proc *SendCmdProc) HandleResponse() {
	for r := range proc.Channel {
		code, content := r.Code, r.Content
		switch proc.status {
		case request_sent:
			if code == "200" {
				proc.status = connected
			}

		case connected:
			if proc.callback != nil {
				proc.callback(code, content)
			}
			proc.Done(true)
		}
	}
}
