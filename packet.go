package ccnet

/************************************************************************

packet.go implements the ccnet packet r/w operation.

type packet struct {}
func writePacket(w *bufio.Writer, pkt *packet) (err error)
func readPacket(r *bufio.Reader) (pkt *packet, err error)

***********************************************************************/

import (
	"bufio"
	"encoding/binary" // for big endian conversion
	"fmt"
	"io"
)

const (
	headerLength = 8 // 8 byte header

	CCNET_MSG_OK        = 0
	CCNET_MSG_HANDSHAKE = 1
	CCNET_MSG_REQUEST   = 2
	CCNET_MSG_RESPONSE  = 3
	CCNET_MSG_UPDATE    = 4
	CCNET_MSG_RELAY     = 5
)

type header struct {
	version uint8
	typ     uint8
	length  uint16
	id      uint32
}

func (hdr *header) toBytes() (buf []byte) {
	buf = make([]byte, headerLength)
	buf[0] = hdr.version
	buf[1] = hdr.typ
	binary.BigEndian.PutUint16(buf[2:4], hdr.length)
	binary.BigEndian.PutUint32(buf[4:8], hdr.id)
	return
}

func headerFromBytes(buf []byte) (hdr *header, err error) {
	if len(buf) != headerLength {
		err = fmt.Errorf("invalid header length %d", len(buf))
		return
	}
	hdr = new(header)
	hdr.version = buf[0]
	hdr.typ = buf[1]
	hdr.length = binary.BigEndian.Uint16(buf[2:4])
	hdr.id = binary.BigEndian.Uint32(buf[4:8])
	return
}

func writeHeader(w *bufio.Writer, hdr *header) (err error) {
	buf := hdr.toBytes()
	n, err := w.Write(buf)
	if err != nil {
		return
	}

	if n != headerLength {
		err = fmt.Errorf("only write %d bytes of %d", n, headerLength)
		return
	}
	return
}

func readHeader(r io.Reader) (hdr *header, err error) {
	buf := make([]byte, headerLength)
	n, err := io.ReadFull(r, buf)
	if err != nil {
		return
	}

	if n != headerLength {
		err = fmt.Errorf("only read %d of %d bytes", n, headerLength)
		return
	}

	hdr, err = headerFromBytes(buf)
	return
}

type packet struct {
	typ  uint8
	id   int
	body []byte
}

func readPacket(r io.Reader) (pkt *packet, err error) {
	hdr, err := readHeader(r)
	if err != nil {
		return
	}
	body := make([]byte, int(hdr.length))
	n, err := io.ReadFull(r, body)
	if err != nil {
		return
	} else if n != int(hdr.length) {
		err = fmt.Errorf("read only %d bytes of %d", n, hdr.length)
		return
	}

	pkt = &packet{
		typ:  hdr.typ,
		id:   int(hdr.id),
		body: body,
	}

	return
}

func writePacket(w *bufio.Writer, pkt *packet) (err error) {
	hdr := header{
		version: 1,
		typ:     pkt.typ,
		length:  uint16(len(pkt.body)),
		id:      uint32(pkt.id),
	}

	err = writeHeader(w, &hdr)
	if err != nil {
		return
	}

	n, err := w.Write(pkt.body)
	if err != nil {
		return
	}

	if n != int(hdr.length) {
		err = fmt.Errorf("only write %d bytes of %d", n, headerLength)
		return
	}

	if err = w.Flush(); err != nil {
		return
	}

	return
}
