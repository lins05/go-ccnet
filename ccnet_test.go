package ccnet

/************************************************************************

 In this test, we first create a sync and an async client. Then we register an
 rpcserver-proc with the async client, then register an `strlen' rpc function.
 After that, we goto the async main loop in another goroutine, and access the
 rpc service with the sync client in the main goroutine.

 For this test case to run, you need a ccnet daemon running at port
 13419(default port).

************************************************************************/

import (
	"ccnet/searpc"
	"fmt"
	"runtime"
	"testing"
)

var _ = fmt.Println

func TestCcnet(t *testing.T) {
	runtime.GOMAXPROCS(2)
	var err error
	port := 63419

	asyncClient := NewAsyncClient(port)

	err = asyncClient.ConnectDaemon()
	if err != nil {
		t.Error(err)
		return
	}

	strlen := func(s string) (int, error) {
		fmt.Println("[strlen] len(s) =", len(s))
		return len(s), nil
	}

	strmul := func(s string, n int) (string, error) {
		ret := ""
		for i := 0; i < n; i++ {
			ret += s
		}
		return ret, nil
	}

	searpc.CreateService("go-rpcserver")
	searpc.RegisterFunction("go-rpcserver", "strlen", strlen)
	searpc.RegisterFunction("go-rpcserver", "strmul", strmul)

	asyncClient.RegisterService("go-rpcserver", "basic", RpcServerProc{}, nil)

	syncClient := NewSyncClient(port)
	err = syncClient.ConnectDaemon()
	if err != nil {
		t.Error(err)
		return
	}

	// serve ccnet in another goroutine
	go asyncClient.MainLoop()

	rpcClient := syncClient.MakeSearpcClient("go-rpcserver")

	str := "hello world"
	l, err := rpcClient.CallRemoteFunction_Int("strlen", str)
	if err != nil {
		t.Error(err)
		return
	}
	if l != len(str) {
		t.Error("incorrect rpc response")
	}

	n := 1000
	ss := "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
	for len(ss) < 7000 {
		ss += ss
	}

	longStr, err := rpcClient.CallRemoteFunction_String("strmul", ss, n)
	if err != nil {
		t.Error(err)
		return
	}
	if len(longStr) != len(ss)*n {
		t.Error("wrong strmul rpc result")
	}

	n = 7000000
	l, err = rpcClient.CallRemoteFunction_Int("strlen", longStr[:n])
	if err != nil {
		t.Error(err)
		return
	}

	if l != n {
		t.Error("incorrect rpc response")
	}
}
