package searpc

import (
	"encoding/json"
	"fmt"
)

func doMarshal(args ...interface{}) (data []byte, err error) {
	data, err = json.Marshal(args)
	return
}

type intResult struct {
	Ret     int    `json:"ret"`
	ErrCode int    `json:"err_code"`
	ErrMsg  string `json:"err_msg"`
}

type stringResult struct {
	Ret     string `json:"ret"`
	ErrCode int    `json:"err_code"`
	ErrMsg  string `json:"err_msg"`
}

type objectResult struct {
	Ret     map[string]interface{} `json:"ret"`
	ErrCode int                    `json:"err_code"`
	ErrMsg  string                 `json:"err_msg"`
}

type objlistResult struct {
	Ret     []map[string]interface{} `json:"ret"`
	ErrCode int                      `json:"err_code"`
	ErrMsg  string                   `json:"err_msg"`
}

func getError(v interface{}) (err error) {
	switch value := v.(type) {
	case *intResult:
		if value.ErrCode > 0 {
			err = fmt.Errorf("%d %s", value.ErrCode, value.ErrMsg)
			return
		}
	case *stringResult:
		if value.ErrCode > 0 {
			err = fmt.Errorf("%d %s", value.ErrCode, value.ErrMsg)
			return
		}
	case *objectResult:
		if value.ErrCode > 0 {
			err = fmt.Errorf("%d %s", value.ErrCode, value.ErrMsg)
			return
		}
	case *objlistResult:
		if value.ErrCode > 0 {
			err = fmt.Errorf("%d %s", value.ErrCode, value.ErrMsg)
			return
		}
	}
	return
}
