package searpc

// client.go implements searpc client side API.

import (
	"encoding/json"
	"fmt"
)

type Transporter interface {
	Transport(input []byte) (output []byte, err error)
}

type Client struct {
	tx Transporter
}

func NewClient(tx Transporter) *Client {
	return &Client{tx}
}

func (c *Client) callRemoteFunction(retType string, args ...interface{}) (result interface{}, err error) {
	fcallData, err := doMarshal(args...)
	if err != nil {
		return
	}

	retData, err := c.tx.Transport(fcallData)
	if err != nil {
		return
	}

	var ret interface{}

	switch retType {
	case "int":
		ret = &intResult{}
	case "string":
		ret = &stringResult{}
	case "object":
		ret = &objectResult{}
	case "objlist":
		ret = &objlistResult{}
	default:
		err = fmt.Errorf("invalid ret type %s", retType)
		return
	}

	err = json.Unmarshal(retData, ret)
	if err != nil {
		return
	}

	err = getError(ret)
	if err != nil {
		return
	}

	switch value := ret.(type) {
	case *intResult:
		result = value.Ret
	case *stringResult:
		result = value.Ret
	case *objectResult:
		result = value.Ret
	case *objlistResult:
		result = value.Ret
	}

	return
}

func (c *Client) CallRemoteFunction_Int(args ...interface{}) (result int, err error) {
	v, err := c.callRemoteFunction("int", args...)
	if err != nil {
		return
	}

	result, ok := v.(int)
	if !ok {
		err = fmt.Errorf("bad response %v, expected int", v)
		return
	}

	return
}

func (c *Client) CallRemoteFunction_String(args ...interface{}) (result string, err error) {
	v, err := c.callRemoteFunction("string", args...)
	if err != nil {
		return
	}

	result, ok := v.(string)
	if !ok {
		err = fmt.Errorf("bad response %v, expected string", v)
		return
	}

	return
}

func (c *Client) CallRemoteFunction_Object(args ...interface{}) (result map[string]interface{}, err error) {
	v, err := c.callRemoteFunction("object", args...)
	if err != nil {
		return
	}

	result, ok := v.(map[string]interface{})
	if !ok {
		err = fmt.Errorf("bad response %v, expected object", v)
		return
	}

	return
}

func (c *Client) CallRemoteFunction_Objlist(args ...interface{}) (result []map[string]interface{}, err error) {
	v, err := c.callRemoteFunction("objlist", args...)
	if err != nil {
		return
	}

	result, ok := v.([]map[string]interface{})
	if !ok {
		err = fmt.Errorf("bad response %v, expected objlist", v)
		return
	}

	return
}
