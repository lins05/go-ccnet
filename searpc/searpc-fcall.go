package searpc

/*
 * fcall.go uses golang's reflect feature to get a function's signature at
 * runtime, and call it with given args, if the signature matches with args.
 */

import (
	"errors"
	"fmt"
	"reflect"
)

var errWrongArgNumber = errors.New("wrong number of args")
var errWrongArgType = errors.New("wrong arg types")
var errNotFunction = errors.New("not a function")

/* a valid searpc function need to:
 *
 * 1. take only int/string params
 * 2. returns two value. The first must be a int/string/map/array of map, and
 *    the second must be an error value.
 */
func isValidSearpcFunc(f interface{}) error {
	fType := reflect.TypeOf(f)
	if fType.Kind() != reflect.Func {
		return errNotFunction
	}

	nOut := fType.NumOut()
	if nOut != 2 {
		return fmt.Errorf("return values must be 2(%d)", nOut)
	}

	outType := fType.Out(0)
	if !isValidSearpcOutType(outType) {
		return fmt.Errorf("invalid return type: %s", outType)
	}

	nIn := fType.NumIn()
	// check arg types
	for i := 0; i < nIn; i++ {
		iKind := fType.In(i).Kind()
		if iKind != reflect.String && iKind != reflect.Int {
			return fmt.Errorf("wrong param type %s", iKind)
		}
	}

	return nil
}

func isValidSearpcOutType(outType reflect.Type) bool {
	switch outType.Kind() {
	case reflect.Int:
		return true
	case reflect.String:
		return true
	case reflect.Map:
		if outType.Key().Kind() != reflect.String || outType.Elem().Kind() != reflect.Interface {
			return false
		}

		return true
	case reflect.Array, reflect.Slice:
		elemType := outType.Elem()
		if elemType.Kind() != reflect.Map {
			return false
		}
		if elemType.Key().Kind() != reflect.String || elemType.Elem().Kind() != reflect.Interface {
			return false
		}
		return true
	}

	return false
}

func callFunc(f interface{}, args []interface{}) (retType string, answer interface{}, err error) {
	fType := reflect.TypeOf(f)
	// check arg numbers
	n := len(args)
	if fType.NumIn() != n {
		err = errWrongArgNumber
		return
	}

	// check arg types
	for i := 0; i < n; i++ {
		iKind := fType.In(i).Kind()
		argKind := reflect.TypeOf(args[i]).Kind()

		if argKind == reflect.Float64 && iKind == reflect.Int {
			args[i] = int(args[i].(float64))

		} else if argKind != iKind {
			fmt.Println(argKind, iKind)
			err = errWrongArgType
			return
		}
	}

	var vargs []reflect.Value = nil
	if n > 0 {
		vargs = make([]reflect.Value, n)
		for i := 0; i < n; i++ {
			vargs[i] = reflect.ValueOf(args[i])
		}
	}

	var out []reflect.Value
	// execute the function
	vf := reflect.ValueOf(f)
	out = vf.Call(vargs)
	if len(out) != 2 {
		// impossible
		err = fmt.Errorf("wrong return value number")
		return
	}

	answer = out[0].Interface()
	verr := out[1].Interface()
	// when the server function returns nil error, out[1].Interface() is nil too
	if verr != nil {
		var ok bool
		err, ok = out[1].Interface().(error)
		if !ok {
			fmt.Println(out[1].Interface())
			err = fmt.Errorf("wrong return value type %s", out[1].Type().Kind())
			return
		}
	}

	kind := reflect.TypeOf(answer).Kind()
	switch kind {
	case reflect.Int:
		retType = "int"
	case reflect.String:
		retType = "string"
	case reflect.Map:
		retType = "object"
	case reflect.Array:
		retType = "objlist"
	case reflect.Slice:
		retType = "objlist"
	default:
		err = fmt.Errorf("wrong return value type: %s", kind)
		return
	}

	return
}
