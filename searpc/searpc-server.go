package searpc

/************************************************************************
 server.go implements searpc server side API.

 func CreateService(name string) (err error)
 func RegisterFunction(svcName, funName string, f interface{}) (err error)
 func ServerCallFunction(svcName string, fcallData []byte) []byte

************************************************************************/

import (
	"encoding/json"
	"fmt"
)

type service struct {
	name      string
	funcTable map[string]interface{}
}

var serviceTable = make(map[string]*service)

func CreateService(name string) (err error) {
	_, existed := serviceTable[name]
	if existed {
		err = fmt.Errorf("service %s already existed", name)
		return
	}

	svc := &service{
		name:      name,
		funcTable: make(map[string]interface{}),
	}

	serviceTable[name] = svc
	return
}

func RegisterFunction(svcName, funName string, f interface{}) (err error) {
	if err = isValidSearpcFunc(f); err != nil {
		fmt.Println("register function error:", err)
		return
	}

	svc := serviceTable[svcName]
	if svc == nil {
		err = fmt.Errorf("unknown service %s", svcName)
		return
	}

	_, existed := svc.funcTable[funName]
	if existed {
		err = fmt.Errorf("already has function %s in service %s\n", funName, svcName)
		return
	}

	svc.funcTable[funName] = f
	return
}

func serverCallFunction(svcName string, fcallData []byte) (retType string, answer interface{}, err error) {
	svc := serviceTable[svcName]
	if svc == nil {
		err = fmt.Errorf("No such service: \"%s\"", svcName)
		return
	}

	args := make([]interface{}, 0, 100) // big enough to hold all args
	err = json.Unmarshal(fcallData, &args)
	if err != nil {
		fmt.Println("error when unmarshal fcallData:", err)
		return
	} else if len(args) < 1 {
		err = fmt.Errorf("bad request format")
		return
	}

	funName, ok := args[0].(string)
	if !ok {
		err = fmt.Errorf("bad function name \"%v\"", args[0])
		return
	}

	f := svc.funcTable[funName]
	if f == nil {
		err = fmt.Errorf("No function \"%s\" in service \"%s\"", funName, svcName)
		return
	}

	if len(args) > 1 {
		args = args[1:]
	} else {
		args = nil
	}

	retType, answer, err = callFunc(f, args)
	if err != nil {
		return
	}

	return
}

func ServerCallFunction(svcName string, fcallData []byte) []byte {
	retType, v, err := serverCallFunction(svcName, fcallData)
	if err != nil {
		return errToJson(err)
	}

	switch retType {
	case "int":
		return fretInt(v)
	case "string":
		return fretString(v)
	case "object":
		return fretObject(v)
	case "objlist":
		return fretObjlist(v)
	}

	return internalError // place holder
}
