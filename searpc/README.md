What is searpc
====
[libsearpc](https://github.com/lins05/libsearpc) is a simple and easy-to-use C language RPC framework (including both server side & client side) based on GObject System

What is go-searpc
===

go-searpc is the [golang](www.golang.org) implementation of libsearpc.
