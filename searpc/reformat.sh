#!/bin/bash

if [[ $# > 0 ]]; then
    gofmt -w $1
else
    gofmt -w *.go
fi
