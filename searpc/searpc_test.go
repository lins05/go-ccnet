package searpc

import (
	"fmt"
	"testing"
)

func doStrlen(s string) (int, error) {
	return len(s), nil
}

func doStrDup(s string) (string, error) {
	return s, nil
}

func doStudent(name string, age int) ([]map[string]interface{}, error) {
	stu := []map[string]interface{}{
		{
			"name": "hello" + name,
			"age":  age + age,
		},
		{
			"name": name + name,
			"age":  age - 1,
		},
	}
	return stu, nil
}

type tX struct{}

// tX implemnts the Transporter interface 
func (t tX) Transport(fcallData []byte) (ret []byte, err error) {
	ret = ServerCallFunction("test-rpc", fcallData)
	return
}

func TestSearpc(t *testing.T) {
	c := NewClient(tX{})

	err := CreateService("test-rpc")
	if err != nil {
		t.Error(err)
		return
	}

	err = RegisterFunction("test-rpc", "strlen", doStrlen)
	if err != nil {
		t.Error(err)
		return
	}

	err = RegisterFunction("test-rpc", "strdup", doStrDup)
	if err != nil {
		t.Error(err)
		return
	}

	err = RegisterFunction("test-rpc", "doStudent", doStudent)
	if err != nil {
		t.Error(err)
		return
	}

	testStr := "hello searpc"
	ret, err := c.CallRemoteFunction_Int("strlen", testStr)
	if err != nil {
		t.Error(err)
		return
	}

	if ret != len(testStr) {
		t.Error("bad response", ret)
	}

	ret2, err := c.CallRemoteFunction_Objlist("doStudent", "lin", 24)
	if err != nil {
		t.Error(err)
		return
	}

	fmt.Println(ret2)

	ret3, err := c.CallRemoteFunction_String("strdup", "lin")
	if err != nil {
		t.Error(err)
		return
	}

	fmt.Println(ret3)

}
