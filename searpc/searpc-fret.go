package searpc

import (
	"encoding/json"
	"fmt"
)

var internalError []byte = []byte(`{"err_code":500,"err_msg":"internal error"}`)

type resultError struct {
	Err_code int `json:"err_code"`
	Err_msg  string `json:"err_msg"`
}

func errToJson(e error) []byte {
	ret := &resultError{
		Err_code: 500,
		Err_msg:  e.Error(),
	}

	buf, err := json.Marshal(ret)
	if err != nil {
		fmt.Println("failed to marshal an error:", err)
		return internalError
	}

	return buf
}

type intResultNoErr struct {
	Ret int `json:"ret"`
}

type stringResultNoErr struct {
	Ret string `json:"ret"`
}

type objectResultNoErr struct {
	Ret map[string]interface{} `json:"ret"`
}

type objlistResultNoErr struct {
	Ret []map[string]interface{} `json:"ret"`
}

func fretInt(v interface{}) []byte {
	val, ok := v.(int)
	if !ok {
		return internalError
	}

	ret := &intResultNoErr{
		Ret: val,
	}

	buf, err := json.Marshal(ret)
	if err != nil {
		return internalError
	}

	return buf
}

func fretString(v interface{}) []byte {
	val, ok := v.(string)
	if !ok {
		return internalError
	}

	ret := &stringResultNoErr{
		Ret: val,
	}

	buf, err := json.Marshal(ret)
	if err != nil {
		fmt.Println(err)
		return internalError
	}

	return buf
}

func fretObject(v interface{}) []byte {
	val, ok := v.(map[string]interface{})
	if !ok {
		return internalError
	}

	ret := &objectResultNoErr{
		Ret: val,
	}

	buf, err := json.Marshal(ret)
	if err != nil {
		return internalError
	}

	return buf
}

func fretObjlist(v interface{}) []byte {
	val, ok := v.([]map[string]interface{})
	if !ok {
		return internalError
	}

	ret := &objlistResultNoErr{
		Ret: val,
	}

	buf, err := json.Marshal(ret)
	if err != nil {
		return internalError
	}

	return buf
}
