package ccnet

// helper functions

func toPacketId(id int) int {
	return (id & request_id_mask)
}

func toSlaveId(id int) int {
	return int(uint32(id) | slave_mask)
}

func toMasterId(id int) int {
	return (id & request_id_mask)
}

func isSlaveId(id int) bool {
	return (uint32(id) & slave_mask) != 0
}
