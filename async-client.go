package ccnet

// Asynchronous mode client. see client.go for detailed comments.

import (
	"fmt"
	"reflect"
	"strings"
	"sync"
)

type AsyncClient struct {
	Client
	procTypes   map[string]reflect.Type
	masterProcs map[int]IMasterProcessor
	slaveProcs  map[int]ISlaveProcessor
	lock        sync.Mutex // for mulitple goroutine access the processors map
	idChan      chan int
}

func NewAsyncClient(daemon_port int) (c *AsyncClient) {
	c = new(AsyncClient)
	c.daemon_port = daemon_port
	c.procTypes = make(map[string]reflect.Type)
	c.masterProcs = make(map[int]IMasterProcessor)
	c.slaveProcs = make(map[int]ISlaveProcessor)

	ch := make(chan int)
	go func() {
		i := 1000
		for {
			i++
			ch <- i
		}
	}()
	c.idChan = ch

	c.registerProcessors()
	return
}

func (c *AsyncClient) registerProcessors() {
	c.RegisterProcessor("send-cmd", SendCmdProc{})
}

func (c *AsyncClient) addMasterProcessor(id int, proc IMasterProcessor) {
	c.lock.Lock()
	c.masterProcs[id] = proc
	c.lock.Unlock()
}

func (c *AsyncClient) addSlaveProcessor(id int, proc ISlaveProcessor) {
	c.lock.Lock()
	c.slaveProcs[id] = proc
	c.lock.Unlock()
}

func (c *AsyncClient) removeProcessor(id int) {
	c.lock.Lock()
	if isSlaveId(id) {
		delete(c.slaveProcs, id)
	} else {
		delete(c.masterProcs, id)
	}
	c.lock.Unlock()
}

func (c *AsyncClient) getMasterProcessor(id int) IMasterProcessor {
	c.lock.Lock()
	proc := c.masterProcs[id]
	c.lock.Unlock()
	return proc
}

func (c *AsyncClient) getSlaveProcessor(id int) ISlaveProcessor {
	c.lock.Lock()
	id = toSlaveId(id)
	proc := c.slaveProcs[id]
	c.lock.Unlock()
	return proc
}

var parseUpdate = parseResponse

func (c *AsyncClient) handlePacket(pkt *packet) {
	switch pkt.typ {
	case CCNET_MSG_REQUEST:
		c.handleRequest(pkt.id, string(pkt.body))

	case CCNET_MSG_UPDATE:
		code, code_msg, content, err := parseUpdate(pkt.body)
		if err != nil {
			fmt.Println(err)
		} else {
			c.handleUpdate(pkt.id, code, code_msg, content)
		}

	case CCNET_MSG_RESPONSE:
		code, code_msg, content, err := parseResponse(pkt.body)
		if err != nil {
			fmt.Println(err)
		} else {
			c.handleResponse(pkt.id, code, code_msg, content)
		}
	default:
		fmt.Println("unknown packet type %d\n", pkt.typ)
	}
}

func (c *AsyncClient) handleRequest(id int, request string) {
	commands := strings.Split(request, " ")
	c.createSlaveProcessor(id, commands)
}

func (c *AsyncClient) handleUpdate(id int, code, code_msg string, content []byte) {
	proc := c.getSlaveProcessor(id)
	if proc == nil {
		if code != SC_PROC_DEAD {
			c.SendResponse(id, SC_PROC_DEAD, SS_PROC_DEAD, nil)
			return
		}

	} else {
		// First handle common update code, then call specific processor's
		// HandleUpdate method if needed.
		if code[0] == '5' {
			fmt.Printf("shutdown processor %s(%d): %s %s\n",
				proc.GetProcessor().Name, toPacketId(proc.GetProcessor().Id), code, code_msg)
			switch code {
			case SC_UNKNOWN_SERVICE:
				proc.ShutDown(PROC_NO_SERVICE)
			case SC_PERM_ERR:
				proc.ShutDown(PROC_PERM_ERR)
			default:
				proc.ShutDown(PROC_BAD_RESP)
			}

		} else if code == SC_PROC_KEEPALIVE {
			proc.SendResponse(SC_PROC_KEEPALIVE, SS_PROC_KEEPALIVE, nil)

		} else if code == SC_PROC_DEAD {
			fmt.Printf("shutdown processor %s(-%d): when peer(%.8s) processor is dead\n",
				proc.GetProcessor().Name, toPacketId(proc.GetProcessor().Id), proc.GetProcessor().PeerId)
			proc.ShutDown(PROC_REMOTE_DEAD)

		} else if code == SC_PROC_DONE {
			// master is done, so i am done
			proc.Done(true)
		} else {
			proc.ReceiveUpdate(&Update{code, code_msg, content})
		}
	}
}

func (c *AsyncClient) handleResponse(id int, code, code_msg string, content []byte) {
	proc := c.getMasterProcessor(id)
	if proc == nil {
		if code != SC_PROC_DEAD {
			c.SendUpdate(id, SC_PROC_DEAD, SS_PROC_DEAD, nil)
			return
		}

	} else {
		// First handle common response code, then call specific processor's
		// HandleResponse method if needed.
		if code[0] == '5' {
			switch code {
			case SC_UNKNOWN_SERVICE:
				proc.ShutDown(PROC_NO_SERVICE)
			case SC_PERM_ERR:
				proc.ShutDown(PROC_PERM_ERR)
			default:
				proc.ShutDown(PROC_BAD_RESP)
			}

		} else if code == SC_PROC_KEEPALIVE {
			proc.SendUpdate(SC_PROC_KEEPALIVE, SS_PROC_KEEPALIVE, nil)
		} else if code == SC_PROC_DEAD {
			fmt.Printf("shutdown processor %s(%d): when peer(%.8s) processor is dead\n",
				proc.GetProcessor().Name, toPacketId(proc.GetProcessor().Id), proc.GetProcessor().PeerId)
			proc.ShutDown(PROC_REMOTE_DEAD)
		} else {
			proc.ReceiveResponse(&Response{code, code_msg, content})
		}
	}
}

func (c *AsyncClient) CreateMasterProcessor(name string) IMasterProcessor {
	id := c.GetRequestId()
	procType, ok := c.procTypes[name]
	if !ok {
		fmt.Println("unknown processor %s\n", name)
		return nil
	}

	proc := reflect.New(procType).Interface().(IMasterProcessor)
	proc.Init(name, id, c.MyPeerId, c)
	go proc.HandleResponse()
	c.addMasterProcessor(id, proc)
	return proc
}

func (c *AsyncClient) createSlaveProcessor(id int, argv []string) {
	peer_id := c.MyPeerId
	if argv[0] == "remote" {
		if len(argv) < 3 {
			fmt.Println("invalid request:", argv)
			return
		}

		peer_id = argv[1]
		argv = argv[2:]
	}

	name := argv[0]

	procType, ok := c.procTypes[name]
	if !ok {
		fmt.Printf("unknown processor type: %s\n", name)
		return
	} else {
		proc := reflect.New(procType).Interface().(ISlaveProcessor)
		proc.Init(name, toSlaveId(id), peer_id, c)

		c.addSlaveProcessor(toSlaveId(id), proc)
		go proc.HandleUpdate()
		if len(argv) > 1 {
			proc.Start(argv[1:])
		} else {
			proc.Start(nil)
		}
	}
}

func (c *AsyncClient) SendCmd(cmd string, cb func(bool)) {
	cb_wrapper := func(code string, content []byte) {
		if cb != nil {
			cb(code == SC_SERV_EXISTED)
		}
	}

	proc := c.CreateMasterProcessor("send-cmd")
	sproc, ok := proc.(*SendCmdProc)

	if !ok {
		fmt.Println("wrong type of send-cmd proc")
	}

	sproc.SetCallBack(cb_wrapper)
	sproc.Start(nil)
	sproc.SendCmd(cmd)
}

func (c *AsyncClient) RegisterProcessor(name string, proc interface{}) (err error) {
	procType := reflect.TypeOf(proc)
	_, existed := c.procTypes[name]
	if !existed {
		c.procTypes[name] = procType
	} else {
		fmt.Printf("can't register already existed processor %s\n", name)
		err = fmt.Errorf("processor %s is already registered", name)
	}
	return
}

func (c *AsyncClient) RegisterService(service, group string, proc interface{}, callback func(bool)) (err error) {
	err = c.RegisterProcessor(service, proc)
	if err != nil {
		return
	}
	cmd := fmt.Sprintf("register-service %s %s", service, group)
	c.SendCmd(cmd, callback)
	return
}

func (c *AsyncClient) MainLoop() error {
	for {
		pkt, err := readPacket(c.reader)
		if err != nil {
			return err
		}
		c.handlePacket(pkt)
	}
	return nil
}

func (c *AsyncClient) GetRequestId() int {
	return <-c.idChan
}
