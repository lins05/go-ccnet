package ccnet

// An echo processor sends back whatever it receives. Just for test.

import ()

type EchoProc struct {
	SlaveProcessor
}

func (proc *EchoProc) Start(argv []string) (err error) {
	buf := ""
	for _, arg := range argv {
		buf += arg
	}
	if len(buf) == 0 {
		buf = "hello-golang"
	}
	proc.SendResponse("300", "XXX", []byte(buf))
	return
}
