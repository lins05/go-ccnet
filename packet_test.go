package ccnet

import (
	"bufio"
	"bytes"
	"fmt"
	"testing"
)

var _ = fmt.Println

func TestPacket(t *testing.T) {
	buf := new(bytes.Buffer)
	pkt := &packet{
		typ:  1,
		id:   1001,
		body: []byte{1, 2, 3, 2, 1},
	}

	reader := bufio.NewReader(buf)
	writer := bufio.NewWriter(buf)

	if err := writePacket(writer, pkt); err != nil {
		t.Errorf(err.Error())
		return
	}

	pkt2, err := readPacket(reader)
	if err != nil {
		t.Errorf(err.Error())
		return
	}

	if pkt.id != pkt2.id || pkt.typ != pkt2.typ || len(pkt.body) != len(pkt2.body) {
		t.Errorf("data mismatch")
		return
	}

	if !bytes.Equal(pkt.body, pkt2.body) {
		t.Errorf("data mismatch")
		return
	}
}
